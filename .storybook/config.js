import { configure, addDecorator } from '@storybook/react';
import { withKnobs } from "@storybook/addon-knobs"
import { withInfo } from '@storybook/addon-info';

addDecorator(withKnobs);
addDecorator(withInfo);

//function loadStories() {
//    require('../stories/index.js');
// You can require as many stories as you need.
//}


function loadStories() {
    const req = require.context("../src", true, /.stories.js$/);
    req.keys().forEach(req);
}

configure(loadStories, module);