import React from "react";
import { storiesOf } from "@storybook/react";
import { text, number, boolean, select } from "@storybook/addon-knobs";
import { ThemeProvider } from "styled-components";
import Theme from '../../Theme';
import Button from "./index";

let theme = Theme;
//theme.button.primary.background = 'grey';


storiesOf("Button", module)
    .add("Primary", () => {
        const children = text("Children", "Default button");
        const disabled = boolean("disabled", false)
        return <Button type="primary" disabled={disabled} theme={theme}>{children}</Button>

    }, {
        info: {
            inline: true, header: false, source: false, text: `
            Základní tlačítko
        ` }
    })
    .add("Secondary", () => {
        const children = text("Children", "tlačítko");
        const disabled = boolean("disabled", false)
        return <ThemeProvider theme={theme}>
            <Button type="secondary" disabled={disabled}>{children}</Button>
        </ThemeProvider>
    }, {
        info: {
            inline: true, header: false, source: false, text: `
            Základní tlačítko
        ` }
    })
    .add("Warning", () => {
        const children = text("Children", "tlačítko");
        const enabled = boolean("Zakázáno", false)
        return <Button type="warning">{children}</Button>;
    }, {
        info: {
            inline: true, header: false, source: false, text: `
            Základní tlačítko
        ` }
    })
    .add("All", () => {
        const children = text("Children", "tlačítko");
        return <React.Fragment>
            <Button type="primary">{children}</Button>
            <Button type="secondary">{children}</Button>
            <Button type="warning">{children}</Button>
        </React.Fragment>
    })