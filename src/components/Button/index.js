import React from 'react'
import PropTypes from 'prop-types';
import styled, { css } from "styled-components";

function getThemeButton(props, key) {
    if (props.theme && props.theme.button && props.theme.button[props.type] && props.theme.button[props.type][key]) {
        return props.theme.button[props.type][key];
    } else {
        return null
    }
}

const StyledButton = styled.button`
    display: flex;
    flex-basis: auto;
    justify-content: center;
    align-items: center;  
    border-radius: 3px;
    border: 1px solid grey;
    padding: 10px 20px;
    color: white;
    font-weight: bold;
    text-transform: uppercase;
    cursor: pointer;
    margin: 20px;
    font-size: 10px;

    &:disabled {
        opacity: 0.5;
        cursor: default;
        background: grey;
        border-color: grey;
    }

    background-color: grey;

    ${p => p.type && css`
        background: ${getThemeButton(p, 'background')};
        border-color: ${getThemeButton(p, 'borderColor')};
    `}

`;




function Button({ type = 'primary', children, disabled = false, ...rest }) {
    return (
        <StyledButton type={type} disabled={disabled} {...rest}>{children}</StyledButton>
    )
}

Button.propTypes = {
    /** Název tlačítka */
    children: PropTypes.string.isRequired,
    /** Typ tlačítka */
    type: PropTypes.oneOf(['primary', 'secondary', 'warning']),
    /** Povoleno / zakázáno */
    disabled: PropTypes.bool
};

export default Button;