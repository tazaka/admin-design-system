# Button
To implement Button component into your project you'll need to add the import:
```jsx
import {Button} from "admin-design-system";
```
After adding import into your project you can use it simply like:
```jsx
<Button>Hello World!</Button>
```