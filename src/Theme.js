const theme = {
    colors: {
        primary: 'red',
        secondary: 'orange'
    },
    button: {
        primary: {
            background: () => theme.colors.primary || 'blue',
            borderColor: () => theme.colors.primary || 'blue',
        },
        secondary: {
            background: () => theme.colors.secondary || 'blue',
            borderColor: () => theme.colors.secondary || 'blue',
        }
    }
}
export default theme;