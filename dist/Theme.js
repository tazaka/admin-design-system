"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var theme = {
  colors: {
    primary: 'red',
    secondary: 'orange'
  },
  button: {
    primary: {
      background: function background() {
        return theme.colors.primary || 'blue';
      },
      borderColor: function borderColor() {
        return theme.colors.primary || 'blue';
      }
    },
    secondary: {
      background: function background() {
        return theme.colors.secondary || 'blue';
      },
      borderColor: function borderColor() {
        return theme.colors.secondary || 'blue';
      }
    }
  }
};
var _default = theme;
exports["default"] = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9UaGVtZS5qcyJdLCJuYW1lcyI6WyJ0aGVtZSIsImNvbG9ycyIsInByaW1hcnkiLCJzZWNvbmRhcnkiLCJidXR0b24iLCJiYWNrZ3JvdW5kIiwiYm9yZGVyQ29sb3IiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQU1BLEtBQUssR0FBRztBQUNWQyxFQUFBQSxNQUFNLEVBQUU7QUFDSkMsSUFBQUEsT0FBTyxFQUFFLEtBREw7QUFFSkMsSUFBQUEsU0FBUyxFQUFFO0FBRlAsR0FERTtBQUtWQyxFQUFBQSxNQUFNLEVBQUU7QUFDSkYsSUFBQUEsT0FBTyxFQUFFO0FBQ0xHLE1BQUFBLFVBQVUsRUFBRTtBQUFBLGVBQU1MLEtBQUssQ0FBQ0MsTUFBTixDQUFhQyxPQUFiLElBQXdCLE1BQTlCO0FBQUEsT0FEUDtBQUVMSSxNQUFBQSxXQUFXLEVBQUU7QUFBQSxlQUFNTixLQUFLLENBQUNDLE1BQU4sQ0FBYUMsT0FBYixJQUF3QixNQUE5QjtBQUFBO0FBRlIsS0FETDtBQUtKQyxJQUFBQSxTQUFTLEVBQUU7QUFDUEUsTUFBQUEsVUFBVSxFQUFFO0FBQUEsZUFBTUwsS0FBSyxDQUFDQyxNQUFOLENBQWFFLFNBQWIsSUFBMEIsTUFBaEM7QUFBQSxPQURMO0FBRVBHLE1BQUFBLFdBQVcsRUFBRTtBQUFBLGVBQU1OLEtBQUssQ0FBQ0MsTUFBTixDQUFhRSxTQUFiLElBQTBCLE1BQWhDO0FBQUE7QUFGTjtBQUxQO0FBTEUsQ0FBZDtlQWdCZUgsSyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHRoZW1lID0ge1xuICAgIGNvbG9yczoge1xuICAgICAgICBwcmltYXJ5OiAncmVkJyxcbiAgICAgICAgc2Vjb25kYXJ5OiAnb3JhbmdlJ1xuICAgIH0sXG4gICAgYnV0dG9uOiB7XG4gICAgICAgIHByaW1hcnk6IHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICgpID0+IHRoZW1lLmNvbG9ycy5wcmltYXJ5IHx8ICdibHVlJyxcbiAgICAgICAgICAgIGJvcmRlckNvbG9yOiAoKSA9PiB0aGVtZS5jb2xvcnMucHJpbWFyeSB8fCAnYmx1ZScsXG4gICAgICAgIH0sXG4gICAgICAgIHNlY29uZGFyeToge1xuICAgICAgICAgICAgYmFja2dyb3VuZDogKCkgPT4gdGhlbWUuY29sb3JzLnNlY29uZGFyeSB8fCAnYmx1ZScsXG4gICAgICAgICAgICBib3JkZXJDb2xvcjogKCkgPT4gdGhlbWUuY29sb3JzLnNlY29uZGFyeSB8fCAnYmx1ZScsXG4gICAgICAgIH1cbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCB0aGVtZTsiXX0=