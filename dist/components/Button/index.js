"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n        background: ", ";\n        border-color: ", ";\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    display: flex;\n    flex-basis: auto;\n    justify-content: center;\n    align-items: center;  \n    border-radius: 3px;\n    border: 1px solid grey;\n    padding: 10px 20px;\n    color: white;\n    font-weight: bold;\n    text-transform: uppercase;\n    cursor: pointer;\n    margin: 20px;\n    font-size: 10px;\n\n    &:disabled {\n        opacity: 0.5;\n        cursor: default;\n        background: grey;\n        border-color: grey;\n    }\n\n    background-color: grey;\n\n    ", "\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function getThemeButton(props, key) {
  if (props.theme && props.theme.button && props.theme.button[props.type] && props.theme.button[props.type][key]) {
    return props.theme.button[props.type][key];
  } else {
    return null;
  }
}

var StyledButton = _styledComponents["default"].button(_templateObject(), function (p) {
  return p.type && (0, _styledComponents.css)(_templateObject2(), getThemeButton(p, 'background'), getThemeButton(p, 'borderColor'));
});

function Button(_ref) {
  var _ref$type = _ref.type,
      type = _ref$type === void 0 ? 'primary' : _ref$type,
      children = _ref.children,
      _ref$disabled = _ref.disabled,
      disabled = _ref$disabled === void 0 ? false : _ref$disabled,
      rest = _objectWithoutProperties(_ref, ["type", "children", "disabled"]);

  return _react["default"].createElement(StyledButton, _extends({
    type: type,
    disabled: disabled
  }, rest), children);
}

Button.propTypes = {
  children: _propTypes["default"].string.isRequired,
  type: _propTypes["default"].oneOf(['primary', 'secondary', 'warning']),
  disabled: _propTypes["default"].bool
};
var _default = Button;
exports["default"] = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL0J1dHRvbi9pbmRleC5qcyJdLCJuYW1lcyI6WyJnZXRUaGVtZUJ1dHRvbiIsInByb3BzIiwia2V5IiwidGhlbWUiLCJidXR0b24iLCJ0eXBlIiwiU3R5bGVkQnV0dG9uIiwic3R5bGVkIiwicCIsImNzcyIsIkJ1dHRvbiIsImNoaWxkcmVuIiwiZGlzYWJsZWQiLCJyZXN0IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsIm9uZU9mIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxTQUFTQSxjQUFULENBQXdCQyxLQUF4QixFQUErQkMsR0FBL0IsRUFBb0M7QUFDaEMsTUFBSUQsS0FBSyxDQUFDRSxLQUFOLElBQWVGLEtBQUssQ0FBQ0UsS0FBTixDQUFZQyxNQUEzQixJQUFxQ0gsS0FBSyxDQUFDRSxLQUFOLENBQVlDLE1BQVosQ0FBbUJILEtBQUssQ0FBQ0ksSUFBekIsQ0FBckMsSUFBdUVKLEtBQUssQ0FBQ0UsS0FBTixDQUFZQyxNQUFaLENBQW1CSCxLQUFLLENBQUNJLElBQXpCLEVBQStCSCxHQUEvQixDQUEzRSxFQUFnSDtBQUM1RyxXQUFPRCxLQUFLLENBQUNFLEtBQU4sQ0FBWUMsTUFBWixDQUFtQkgsS0FBSyxDQUFDSSxJQUF6QixFQUErQkgsR0FBL0IsQ0FBUDtBQUNILEdBRkQsTUFFTztBQUNILFdBQU8sSUFBUDtBQUNIO0FBQ0o7O0FBRUQsSUFBTUksWUFBWSxHQUFHQyw2QkFBT0gsTUFBVixvQkF3QlosVUFBQUksQ0FBQztBQUFBLFNBQUlBLENBQUMsQ0FBQ0gsSUFBRixRQUFVSSxxQkFBVixzQkFDV1QsY0FBYyxDQUFDUSxDQUFELEVBQUksWUFBSixDQUR6QixFQUVhUixjQUFjLENBQUNRLENBQUQsRUFBSSxhQUFKLENBRjNCLENBQUo7QUFBQSxDQXhCVyxDQUFsQjs7QUFrQ0EsU0FBU0UsTUFBVCxPQUEyRTtBQUFBLHVCQUF6REwsSUFBeUQ7QUFBQSxNQUF6REEsSUFBeUQsMEJBQWxELFNBQWtEO0FBQUEsTUFBdkNNLFFBQXVDLFFBQXZDQSxRQUF1QztBQUFBLDJCQUE3QkMsUUFBNkI7QUFBQSxNQUE3QkEsUUFBNkIsOEJBQWxCLEtBQWtCO0FBQUEsTUFBUkMsSUFBUTs7QUFDdkUsU0FDSSxnQ0FBQyxZQUFEO0FBQWMsSUFBQSxJQUFJLEVBQUVSLElBQXBCO0FBQTBCLElBQUEsUUFBUSxFQUFFTztBQUFwQyxLQUFrREMsSUFBbEQsR0FBeURGLFFBQXpELENBREo7QUFHSDs7QUFFREQsTUFBTSxDQUFDSSxTQUFQLEdBQW1CO0FBQ2ZILEVBQUFBLFFBQVEsRUFBRUksc0JBQVVDLE1BQVYsQ0FBaUJDLFVBRFo7QUFFZlosRUFBQUEsSUFBSSxFQUFFVSxzQkFBVUcsS0FBVixDQUFnQixDQUFDLFNBQUQsRUFBWSxXQUFaLEVBQXlCLFNBQXpCLENBQWhCLENBRlM7QUFHZk4sRUFBQUEsUUFBUSxFQUFFRyxzQkFBVUk7QUFITCxDQUFuQjtlQU1lVCxNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQsIHsgY3NzIH0gZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5cbmZ1bmN0aW9uIGdldFRoZW1lQnV0dG9uKHByb3BzLCBrZXkpIHtcbiAgICBpZiAocHJvcHMudGhlbWUgJiYgcHJvcHMudGhlbWUuYnV0dG9uICYmIHByb3BzLnRoZW1lLmJ1dHRvbltwcm9wcy50eXBlXSAmJiBwcm9wcy50aGVtZS5idXR0b25bcHJvcHMudHlwZV1ba2V5XSkge1xuICAgICAgICByZXR1cm4gcHJvcHMudGhlbWUuYnV0dG9uW3Byb3BzLnR5cGVdW2tleV07XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIG51bGxcbiAgICB9XG59XG5cbmNvbnN0IFN0eWxlZEJ1dHRvbiA9IHN0eWxlZC5idXR0b25gXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWJhc2lzOiBhdXRvO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7ICBcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgZ3JleTtcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIG1hcmdpbjogMjBweDtcbiAgICBmb250LXNpemU6IDEwcHg7XG5cbiAgICAmOmRpc2FibGVkIHtcbiAgICAgICAgb3BhY2l0eTogMC41O1xuICAgICAgICBjdXJzb3I6IGRlZmF1bHQ7XG4gICAgICAgIGJhY2tncm91bmQ6IGdyZXk7XG4gICAgICAgIGJvcmRlci1jb2xvcjogZ3JleTtcbiAgICB9XG5cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5O1xuXG4gICAgJHtwID0+IHAudHlwZSAmJiBjc3NgXG4gICAgICAgIGJhY2tncm91bmQ6ICR7Z2V0VGhlbWVCdXR0b24ocCwgJ2JhY2tncm91bmQnKX07XG4gICAgICAgIGJvcmRlci1jb2xvcjogJHtnZXRUaGVtZUJ1dHRvbihwLCAnYm9yZGVyQ29sb3InKX07XG4gICAgYH1cblxuYDtcblxuXG5cblxuZnVuY3Rpb24gQnV0dG9uKHsgdHlwZSA9ICdwcmltYXJ5JywgY2hpbGRyZW4sIGRpc2FibGVkID0gZmFsc2UsIC4uLnJlc3QgfSkge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxTdHlsZWRCdXR0b24gdHlwZT17dHlwZX0gZGlzYWJsZWQ9e2Rpc2FibGVkfSB7Li4ucmVzdH0+e2NoaWxkcmVufTwvU3R5bGVkQnV0dG9uPlxuICAgIClcbn1cblxuQnV0dG9uLnByb3BUeXBlcyA9IHtcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIHR5cGU6IFByb3BUeXBlcy5vbmVPZihbJ3ByaW1hcnknLCAnc2Vjb25kYXJ5JywgJ3dhcm5pbmcnXSksXG4gICAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBCdXR0b247Il19