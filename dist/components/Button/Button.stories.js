"use strict";

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _addonKnobs = require("@storybook/addon-knobs");

var _styledComponents = require("styled-components");

var _Theme = _interopRequireDefault(require("../../Theme"));

var _index = _interopRequireDefault(require("./index"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var theme = _Theme["default"]; //theme.button.primary.background = 'grey';

(0, _react2.storiesOf)("Button", module).add("Primary", function () {
  var children = (0, _addonKnobs.text)("Children", "Default button");
  var disabled = (0, _addonKnobs["boolean"])("disabled", false);
  return _react["default"].createElement(_styledComponents.ThemeProvider, {
    theme: theme
  }, _react["default"].createElement(_index["default"], {
    type: "primary",
    disabled: disabled
  }, children));
}, {
  info: {
    inline: true,
    header: false,
    source: false,
    text: "\n            Z\xE1kladn\xED tla\u010D\xEDtko\n        "
  }
}).add("Secondary", function () {
  var children = (0, _addonKnobs.text)("Children", "tlačítko");
  var disabled = (0, _addonKnobs["boolean"])("disabled", false);
  return _react["default"].createElement(_styledComponents.ThemeProvider, {
    theme: theme
  }, _react["default"].createElement(_index["default"], {
    type: "secondary",
    disabled: disabled
  }, children));
}, {
  info: {
    inline: true,
    header: false,
    source: false,
    text: "\n            Z\xE1kladn\xED tla\u010D\xEDtko\n        "
  }
}).add("Warning", function () {
  var children = (0, _addonKnobs.text)("Children", "tlačítko");
  var enabled = (0, _addonKnobs["boolean"])("Zakázáno", false);
  return _react["default"].createElement(_index["default"], {
    type: "warning"
  }, children);
}, {
  info: {
    inline: true,
    header: false,
    source: false,
    text: "\n            Z\xE1kladn\xED tla\u010D\xEDtko\n        "
  }
}).add("All", function () {
  var children = (0, _addonKnobs.text)("Children", "tlačítko");
  return _react["default"].createElement(_react["default"].Fragment, null, _react["default"].createElement(_index["default"], {
    type: "primary"
  }, children), _react["default"].createElement(_index["default"], {
    type: "secondary"
  }, children), _react["default"].createElement(_index["default"], {
    type: "warning"
  }, children));
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21wb25lbnRzL0J1dHRvbi9CdXR0b24uc3Rvcmllcy5qcyJdLCJuYW1lcyI6WyJ0aGVtZSIsIlRoZW1lIiwibW9kdWxlIiwiYWRkIiwiY2hpbGRyZW4iLCJkaXNhYmxlZCIsImluZm8iLCJpbmxpbmUiLCJoZWFkZXIiLCJzb3VyY2UiLCJ0ZXh0IiwiZW5hYmxlZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7OztBQUVBLElBQUlBLEtBQUssR0FBR0MsaUJBQVosQyxDQUNBOztBQUdBLHVCQUFVLFFBQVYsRUFBb0JDLE1BQXBCLEVBQ0tDLEdBREwsQ0FDUyxTQURULEVBQ29CLFlBQU07QUFDbEIsTUFBTUMsUUFBUSxHQUFHLHNCQUFLLFVBQUwsRUFBaUIsZ0JBQWpCLENBQWpCO0FBQ0EsTUFBTUMsUUFBUSxHQUFHLDRCQUFRLFVBQVIsRUFBb0IsS0FBcEIsQ0FBakI7QUFDQSxTQUFPLGdDQUFDLCtCQUFEO0FBQWUsSUFBQSxLQUFLLEVBQUVMO0FBQXRCLEtBQ0gsZ0NBQUMsaUJBQUQ7QUFBUSxJQUFBLElBQUksRUFBQyxTQUFiO0FBQXVCLElBQUEsUUFBUSxFQUFFSztBQUFqQyxLQUE0Q0QsUUFBNUMsQ0FERyxDQUFQO0FBR0gsQ0FQTCxFQU9PO0FBQ0NFLEVBQUFBLElBQUksRUFBRTtBQUNGQyxJQUFBQSxNQUFNLEVBQUUsSUFETjtBQUNZQyxJQUFBQSxNQUFNLEVBQUUsS0FEcEI7QUFDMkJDLElBQUFBLE1BQU0sRUFBRSxLQURuQztBQUMwQ0MsSUFBQUEsSUFBSTtBQUQ5QztBQURQLENBUFAsRUFhS1AsR0FiTCxDQWFTLFdBYlQsRUFhc0IsWUFBTTtBQUNwQixNQUFNQyxRQUFRLEdBQUcsc0JBQUssVUFBTCxFQUFpQixVQUFqQixDQUFqQjtBQUNBLE1BQU1DLFFBQVEsR0FBRyw0QkFBUSxVQUFSLEVBQW9CLEtBQXBCLENBQWpCO0FBQ0EsU0FBTyxnQ0FBQywrQkFBRDtBQUFlLElBQUEsS0FBSyxFQUFFTDtBQUF0QixLQUNILGdDQUFDLGlCQUFEO0FBQVEsSUFBQSxJQUFJLEVBQUMsV0FBYjtBQUF5QixJQUFBLFFBQVEsRUFBRUs7QUFBbkMsS0FBOENELFFBQTlDLENBREcsQ0FBUDtBQUdILENBbkJMLEVBbUJPO0FBQ0NFLEVBQUFBLElBQUksRUFBRTtBQUNGQyxJQUFBQSxNQUFNLEVBQUUsSUFETjtBQUNZQyxJQUFBQSxNQUFNLEVBQUUsS0FEcEI7QUFDMkJDLElBQUFBLE1BQU0sRUFBRSxLQURuQztBQUMwQ0MsSUFBQUEsSUFBSTtBQUQ5QztBQURQLENBbkJQLEVBeUJLUCxHQXpCTCxDQXlCUyxTQXpCVCxFQXlCb0IsWUFBTTtBQUNsQixNQUFNQyxRQUFRLEdBQUcsc0JBQUssVUFBTCxFQUFpQixVQUFqQixDQUFqQjtBQUNBLE1BQU1PLE9BQU8sR0FBRyw0QkFBUSxVQUFSLEVBQW9CLEtBQXBCLENBQWhCO0FBQ0EsU0FBTyxnQ0FBQyxpQkFBRDtBQUFRLElBQUEsSUFBSSxFQUFDO0FBQWIsS0FBd0JQLFFBQXhCLENBQVA7QUFDSCxDQTdCTCxFQTZCTztBQUNDRSxFQUFBQSxJQUFJLEVBQUU7QUFDRkMsSUFBQUEsTUFBTSxFQUFFLElBRE47QUFDWUMsSUFBQUEsTUFBTSxFQUFFLEtBRHBCO0FBQzJCQyxJQUFBQSxNQUFNLEVBQUUsS0FEbkM7QUFDMENDLElBQUFBLElBQUk7QUFEOUM7QUFEUCxDQTdCUCxFQW1DS1AsR0FuQ0wsQ0FtQ1MsS0FuQ1QsRUFtQ2dCLFlBQU07QUFDZCxNQUFNQyxRQUFRLEdBQUcsc0JBQUssVUFBTCxFQUFpQixVQUFqQixDQUFqQjtBQUNBLFNBQU8sZ0NBQUMsaUJBQUQsQ0FBTyxRQUFQLFFBQ0gsZ0NBQUMsaUJBQUQ7QUFBUSxJQUFBLElBQUksRUFBQztBQUFiLEtBQXdCQSxRQUF4QixDQURHLEVBRUgsZ0NBQUMsaUJBQUQ7QUFBUSxJQUFBLElBQUksRUFBQztBQUFiLEtBQTBCQSxRQUExQixDQUZHLEVBR0gsZ0NBQUMsaUJBQUQ7QUFBUSxJQUFBLElBQUksRUFBQztBQUFiLEtBQXdCQSxRQUF4QixDQUhHLENBQVA7QUFLSCxDQTFDTCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IHN0b3JpZXNPZiB9IGZyb20gXCJAc3Rvcnlib29rL3JlYWN0XCI7XG5pbXBvcnQgeyB0ZXh0LCBudW1iZXIsIGJvb2xlYW4sIHNlbGVjdCB9IGZyb20gXCJAc3Rvcnlib29rL2FkZG9uLWtub2JzXCI7XG5pbXBvcnQgeyBUaGVtZVByb3ZpZGVyIH0gZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSAnLi4vLi4vVGhlbWUnO1xuaW1wb3J0IEJ1dHRvbiBmcm9tIFwiLi9pbmRleFwiO1xuXG5sZXQgdGhlbWUgPSBUaGVtZTtcbi8vdGhlbWUuYnV0dG9uLnByaW1hcnkuYmFja2dyb3VuZCA9ICdncmV5JztcblxuXG5zdG9yaWVzT2YoXCJCdXR0b25cIiwgbW9kdWxlKVxuICAgIC5hZGQoXCJQcmltYXJ5XCIsICgpID0+IHtcbiAgICAgICAgY29uc3QgY2hpbGRyZW4gPSB0ZXh0KFwiQ2hpbGRyZW5cIiwgXCJEZWZhdWx0IGJ1dHRvblwiKTtcbiAgICAgICAgY29uc3QgZGlzYWJsZWQgPSBib29sZWFuKFwiZGlzYWJsZWRcIiwgZmFsc2UpXG4gICAgICAgIHJldHVybiA8VGhlbWVQcm92aWRlciB0aGVtZT17dGhlbWV9PlxuICAgICAgICAgICAgPEJ1dHRvbiB0eXBlPVwicHJpbWFyeVwiIGRpc2FibGVkPXtkaXNhYmxlZH0+e2NoaWxkcmVufTwvQnV0dG9uPlxuICAgICAgICA8L1RoZW1lUHJvdmlkZXI+XG4gICAgfSwge1xuICAgICAgICBpbmZvOiB7XG4gICAgICAgICAgICBpbmxpbmU6IHRydWUsIGhlYWRlcjogZmFsc2UsIHNvdXJjZTogZmFsc2UsIHRleHQ6IGBcbiAgICAgICAgICAgIFrDoWtsYWRuw60gdGxhxI3DrXRrb1xuICAgICAgICBgIH1cbiAgICB9KVxuICAgIC5hZGQoXCJTZWNvbmRhcnlcIiwgKCkgPT4ge1xuICAgICAgICBjb25zdCBjaGlsZHJlbiA9IHRleHQoXCJDaGlsZHJlblwiLCBcInRsYcSNw610a29cIik7XG4gICAgICAgIGNvbnN0IGRpc2FibGVkID0gYm9vbGVhbihcImRpc2FibGVkXCIsIGZhbHNlKVxuICAgICAgICByZXR1cm4gPFRoZW1lUHJvdmlkZXIgdGhlbWU9e3RoZW1lfT5cbiAgICAgICAgICAgIDxCdXR0b24gdHlwZT1cInNlY29uZGFyeVwiIGRpc2FibGVkPXtkaXNhYmxlZH0+e2NoaWxkcmVufTwvQnV0dG9uPlxuICAgICAgICA8L1RoZW1lUHJvdmlkZXI+XG4gICAgfSwge1xuICAgICAgICBpbmZvOiB7XG4gICAgICAgICAgICBpbmxpbmU6IHRydWUsIGhlYWRlcjogZmFsc2UsIHNvdXJjZTogZmFsc2UsIHRleHQ6IGBcbiAgICAgICAgICAgIFrDoWtsYWRuw60gdGxhxI3DrXRrb1xuICAgICAgICBgIH1cbiAgICB9KVxuICAgIC5hZGQoXCJXYXJuaW5nXCIsICgpID0+IHtcbiAgICAgICAgY29uc3QgY2hpbGRyZW4gPSB0ZXh0KFwiQ2hpbGRyZW5cIiwgXCJ0bGHEjcOtdGtvXCIpO1xuICAgICAgICBjb25zdCBlbmFibGVkID0gYm9vbGVhbihcIlpha8OhesOhbm9cIiwgZmFsc2UpXG4gICAgICAgIHJldHVybiA8QnV0dG9uIHR5cGU9XCJ3YXJuaW5nXCI+e2NoaWxkcmVufTwvQnV0dG9uPjtcbiAgICB9LCB7XG4gICAgICAgIGluZm86IHtcbiAgICAgICAgICAgIGlubGluZTogdHJ1ZSwgaGVhZGVyOiBmYWxzZSwgc291cmNlOiBmYWxzZSwgdGV4dDogYFxuICAgICAgICAgICAgWsOha2xhZG7DrSB0bGHEjcOtdGtvXG4gICAgICAgIGAgfVxuICAgIH0pXG4gICAgLmFkZChcIkFsbFwiLCAoKSA9PiB7XG4gICAgICAgIGNvbnN0IGNoaWxkcmVuID0gdGV4dChcIkNoaWxkcmVuXCIsIFwidGxhxI3DrXRrb1wiKTtcbiAgICAgICAgcmV0dXJuIDxSZWFjdC5GcmFnbWVudD5cbiAgICAgICAgICAgIDxCdXR0b24gdHlwZT1cInByaW1hcnlcIj57Y2hpbGRyZW59PC9CdXR0b24+XG4gICAgICAgICAgICA8QnV0dG9uIHR5cGU9XCJzZWNvbmRhcnlcIj57Y2hpbGRyZW59PC9CdXR0b24+XG4gICAgICAgICAgICA8QnV0dG9uIHR5cGU9XCJ3YXJuaW5nXCI+e2NoaWxkcmVufTwvQnV0dG9uPlxuICAgICAgICA8L1JlYWN0LkZyYWdtZW50PlxuICAgIH0pIl19